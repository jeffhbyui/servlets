package com.example.servlets;

import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

@WebServlet(name = "helloServlet", value = "/hello-servlet", urlPatterns = {"/Servlet"})
public class HelloServlet extends HttpServlet {
    private String unsupportedMethod;

    public void init() {
        unsupportedMethod = "Not a supported method";
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");

        // Hello
        PrintWriter out = response.getWriter();
        String noun = request.getParameter("noun");
        String adjective = request.getParameter("adjective");
        String pastTenseVerb = request.getParameter("pastTenseVerb");
        out.println("<html><head></head><body>");
        out.println("<h1> MadLibs Result: </h1>");
        out.println("<p> Once there was a " + noun + " who was very " + adjective + " </p>");
        out.println("<p> One day they " + pastTenseVerb + " and the world ended because of it</p>");
        out.println("<p> the end. </p>");
        out.println("</body></html>");
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");

        // Hello
        PrintWriter out = response.getWriter();
        out.println("<html><body>");
        out.println("<h1>" + unsupportedMethod + "</h1>");
        out.println("</body></html>");
    }

    public void destroy() {
    }
}